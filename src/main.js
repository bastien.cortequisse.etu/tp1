/*let name = 'Regina';
let url = `images/${ name.toLowerCase() }.jpg`;
console.log(url);
let html = `<article class="pizzaThumbnail"><a href="${url}"><img src="${url}"/><section>Regina</section></a></article>`;
console.log(html);
document.querySelector('.pageContent').innerHTML = html;
*/

/*const data = ['Regina', 'Napolitaine', 'Spicy'];
let html;
for( i =0 ; i<data.length;i++){
    let name = data[i];
    let url = `images/${ name.toLowerCase() }.jpg`;
    html = `${html}<article class="pizzaThumbnail"><a href="${url}"><img src="${url}"/><section>${data[i]}</section></a></article>`;
}
document.querySelector('.pageContent').innerHTML = html;*/

/*const data = ['Regina', 'Napolitaine', 'Spicy'];
document.querySelector('.pageContent').innerHTML = data.reduce(red,"");

function red(html, elCour){
    let name = elCour;
    let url = `images/${ name.toLowerCase() }.jpg`;
    return `${html}<article class="pizzaThumbnail"><a href="${url}"><img src="${url}"/><section>${elCour}</section></a></article>`;
}*/

const data = [
	{
		name: 'Regina',
		base: 'tomate',
		price_small: 6.5,
		price_large: 9.95,
		image: 'https://images.unsplash.com/photo-1532246420286-127bcd803104?fit=crop&w=500&h=300'
	},
	{
		name: 'Napolitaine',
		base: 'tomate',
		price_small: 6.5,
		price_large: 8.95,
		image: 'https://images.unsplash.com/photo-1562707666-0ef112b353e0?&fit=crop&w=500&h=300'
	},
	{
		name: 'Spicy',
		base: 'crème',
		price_small: 5.5,
		price_large: 8,
		image: 'https://images.unsplash.com/photo-1458642849426-cfb724f15ef7?fit=crop&w=500&h=300',
	}
];
data.sort(function(a, b){
    if(a.name < b.name) { return -1; }
    if(a.name > b.name) { return 1; }
    return 0;
})



let html='';
for(var i =0 ; i<data.length;i++){
    html = `${html}<article class="pizzaThumbnail"><a href="${data[i].image}"><img src="${data[i].image}"/><section><h4>${data[i].name}</h4><ul><li>Prix petit format : ${data[i].price_small} €</li> <li>Prix grand format : ${data[i].price_large} €</li></ul></section></a></article>`;
}
document.querySelector('.pageContent').innerHTML = html;